variable "region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "db_instance_class" {
  description = "RDS instance class"
  default     = "db.t3.micro"
}

variable "db_name" {
  description = "Database name"
  default     = "mydb"
}

variable "db_username" {
  description = "Database username"
  default     = "admin"
}

variable "db_password" {
  description = "Database password"
  default     = "password"
}

variable "replica_count" {
  description = "Number of read replicas"
  default     = 1
}
