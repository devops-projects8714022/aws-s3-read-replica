provider "aws" {
  region = var.region
}

resource "aws_db_instance" "main" {
  identifier          = "mydb-main"
  engine              = "mysql"
  instance_class      = var.db_instance_class
  name                = var.db_name
  username            = var.db_username
  password            = var.db_password
  allocated_storage   = 20
  backup_retention_period = 7
  parameter_group_name    = "default.mysql8.0"
  skip_final_snapshot = true
}

resource "aws_db_instance" "replica" {
  count               = var.replica_count
  identifier          = "mydb-replica-${count.index + 1}"
  instance_class      = var.db_instance_class
  replicate_source_db = aws_db_instance.main.id
  skip_final_snapshot = true
}
