# Документация для настройки MySQL read replica в AWS с использованием Terraform

## Описание

Этот документ описывает шаги по настройке MySQL read replica в AWS с использованием Terraform. Данный пример включает создание основного экземпляра базы данных MySQL и одного или нескольких экземпляров read replica для распределения нагрузки на чтение.

## Файлы конфигурации

Проект Terraform состоит из нескольких файлов конфигурации:

1. `variables.tf` — файл переменных, используемых в конфигурации.
2. `main.tf` — основной файл конфигурации, описывающий ресурсы AWS.

### Файл `variables.tf`

Этот файл содержит определения переменных, которые будут использоваться в конфигурации.

```hcl
variable "region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "db_instance_class" {
  description = "RDS instance class"
  default     = "db.t3.micro"
}

variable "db_name" {
  description = "Database name"
  default     = "mydb"
}

variable "db_username" {
  description = "Database username"
  default     = "admin"
}

variable "db_password" {
  description = "Database password"
  default     = "password"
}

variable "replica_count" {
  description = "Number of read replicas"
  default     = 1
}
```

### Файл `main.tf`

Этот файл описывает создание основного экземпляра базы данных MySQL и read replica.

```hcl
provider "aws" {
  region = var.region
}

resource "aws_db_instance" "main" {
  identifier          = "mydb-main"
  engine              = "mysql"
  instance_class      = var.db_instance_class
  name                = var.db_name
  username            = var.db_username
  password            = var.db_password
  allocated_storage   = 20
  backup_retention_period = 7
  parameter_group_name    = "default.mysql8.0"
  skip_final_snapshot = true
}

resource "aws_db_instance" "replica" {
  count               = var.replica_count
  identifier          = "mydb-replica-${count.index + 1}"
  instance_class      = var.db_instance_class
  replicate_source_db = aws_db_instance.main.id
  skip_final_snapshot = true
}
```

## Шаги по настройке

### 1. Инициализация Terraform

Для начала необходимо инициализировать рабочий каталог Terraform. Это скачает необходимые провайдеры и подготовит конфигурацию.

```sh
terraform init
```

### 2. Применение конфигурации

Примените конфигурацию, чтобы создать основной экземпляр и read replica.

```sh
terraform apply
```

Вам будет предложено подтвердить применение конфигурации. Введите `yes` для продолжения.

## Переменные

- `region`: Регион AWS, в котором будут созданы ресурсы. По умолчанию `us-west-2`.
- `db_instance_class`: Класс экземпляра RDS. По умолчанию `db.t3.micro`.
- `db_name`: Имя базы данных. По умолчанию `mydb`.
- `db_username`: Имя пользователя базы данных. По умолчанию `admin`.
- `db_password`: Пароль пользователя базы данных. По умолчанию `password`.
- `replica_count`: Количество read replica. По умолчанию `1`.

## Ресурсы

- `aws_db_instance.main`: Основной экземпляр базы данных MySQL.
- `aws_db_instance.replica`: Экземпляры read replica MySQL.

## Дополнительные настройки

Вы можете настроить конфигурацию в соответствии с вашими требованиями, добавляя параметры безопасности, настраивая VPC, параметры шифрования и другие опции. Более подробную информацию можно найти в [документации Terraform для ресурса aws_db_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance).

## Заключение

Эта документация описывает основные шаги по созданию MySQL read replica в AWS с использованием Terraform. Следуя приведённым инструкциям, вы сможете настроить репликацию для вашей базы данных, распределив нагрузку на чтение и обеспечив отказоустойчивость вашей системы.